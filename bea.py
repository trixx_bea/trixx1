from flask import Flask

app = Flask(__name__)

@app.route("/")
def ola():
    return "Servidor de Hylson disponível na rede"

app.run(debug=True, host="0.0.0.0", port=4999)
